@testset "RecurringExpense" begin
    @testset "getValuePerDay: no end date" begin
        start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        cost = 1000
        expense = pf.RecurringExpense(cost, period, start_date)

        end_date = start_date + Dates.Month(2)
        dates, costs = pf.getValuePerDay(expense, start_date, end_date)
        @test length(dates) == length(costs)
        for x ∈ costs
            @test x ≈ -cost
        end
    end
    @testset "getValuePerDay: has end date" begin
        start_date = Dates.Date(2019, 1, 1)
        end_date = start_date + Dates.Month(3)
        period = Dates.Month(1)
        cost = 1000
        expense = pf.RecurringExpense(cost, period, start_date, end_date)

        dates, costs = pf.getValuePerDay(expense, start_date, start_date + Dates.Month(6))
        @test length(dates) == 4
        @test length(dates) == length(costs)
        for x ∈ costs
            @test x ≈ -cost
        end

        dates, costs = pf.getValuePerDay(expense, start_date, start_date + Dates.Month(2))
        @test length(dates) == 3
        @test length(dates) == length(costs)
        for x ∈ costs
            @test x ≈ -cost
        end
    end
    @testset "getValuePerYear: single year" begin
        expense_start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        cost = 1000
        expense = pf.RecurringExpense(cost, period, expense_start_date)

        start_date = Dates.Date(2019, 1, 1)
        end_date = start_date + Dates.Year(1) - Dates.Week(1)
        expenses = pf.getValuePerYear(expense, start_date, end_date)

        @test length(expenses) == 1
        @test expenses[2019] == -12000.0
    end
    @testset "getValuePerYear: multiple years" begin
        expense_start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        cost = 1000
        expense = pf.RecurringExpense(cost, period, expense_start_date)

        start_date = Dates.Date(2019, 1, 1)
        end_date = start_date + Dates.Year(2) + Dates.Month(6) - Dates.Week(1)
        expenses = pf.getValuePerYear(expense, start_date, end_date)

        @test length(expenses) == 3
        @test expenses[2019] == -12000.0
        @test expenses[2020] == -12000.0
        @test expenses[2021] == -6000.0
    end
    @testset "getValuePerYear: multiple years, period of 2 months" begin
        expense_start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(2)
        cost = 1000
        expense = pf.RecurringExpense(cost, period, expense_start_date)

        start_date = Dates.Date(2019, 1, 1)
        end_date = start_date + Dates.Year(2) + Dates.Month(6) - Dates.Week(1)
        expenses = pf.getValuePerYear(expense, start_date, end_date)

        @test length(expenses) == 3
        @test expenses[2019] == -6000.0
        @test expenses[2020] == -6000.0
        @test expenses[2021] == -3000.0
    end
end

@testset "SingleExpense" begin
    cost = 100.0
    date = Dates.Date(2020, 1, 1)
    se = pf.SingleExpense(cost, date)

    @test pf.get_start_date(se) == date
    a, b = pf.getValuePerDay(se, date, date)
    @test a[1] == date
    @test b[1] == -cost
end