@testset "Profile Construction" begin
    p1 = pf.PersonalProfile("Joseph", "Joe", Dates.Date(1985, 1, 1))
    p2 = pf.PersonalProfile("Joseph", "Joey", "Joe", Dates.Date(1985, 1, 1))
    p2 = pf.PersonalProfile("Joseph", "Joe", Dates.Date(1985, 1, 1), 65, 90)
end
@testset "Portfolio" begin
    @testset "Mapping of expense to assets" begin
        e2a = Dict{String,String}()
        e2a["rent"] = "checking"
        e2a["food"] = "checking"
        e2a["down payment"] = "savings"
        expenses = ["rent", "food", "down payment", "spotify", "alcohol"]
        assets = ["checking", "savings", "retirement"]

        @test pf.validateExpenseToAssetMapping(e2a, expenses, assets) == true
        e2a["rent"] = "checkings"
        @test pf.validateExpenseToAssetMapping(e2a, expenses, assets) == false
        e2a["rent"] = "checking"
        @test pf.validateExpenseToAssetMapping(e2a, expenses, assets) == true
        expenses = ["food", "down payment", "spotify", "alcohol"]
        @test pf.validateExpenseToAssetMapping(e2a, expenses, assets) == false
    end
end