@testset "Simple Simulation" begin
    start_date = Dates.Date(2019, 1, 1)
    period = Dates.Month(1)
    duration = Dates.Month(4)
    rent = 1000
    starting_value = 5000

    # approximately 24000 after tax
    salary = pf.SimpleSalary(28356.11, 0.03, period, start_date)

    accounts = Dict{String,pf.BankAccount}()
    accounts["Checking"] = pf.BankAccount(starting_value, 0.0, Dates.Day(1))

    expenses = Dict{String,pf.RecurringExpense}()
    expenses["Rent"] = pf.RecurringExpense(1000, period, start_date)
    headphone_cost = 100
    expenses["Headphones"] = pf.SingleExpense(headphone_cost, start_date + Dates.Day(5))

    my_portfolio = pf.SimplePortfolio(salary, accounts, expenses)
    simout = pf.simulateBudget(my_portfolio, start_date=start_date, duration=duration)

    @test simout.returns[end] > accounts["Checking"].principal
    @test round(simout.returns[end]) ≈ 1000 * (Dates.value(duration) + 1) + starting_value - headphone_cost
    pf.summary(my_portfolio, start_date=start_date)
end
@testset "Retirement Simulation" begin
    start_date = Dates.Date(2019, 1, 1)
    birthday = Dates.Date(1950, 1, 1)
    retirement_age = 80
    life_expectancy = 80
    retirement_date = birthday + Dates.Year(retirement_age)
    profile = pf.PersonalProfile("Joe", "Joey", "Smith", birthday, retirement_age, life_expectancy)

    salary_growth_rate = 0.00
    salary_val = 10000.0
    salary = pf.SimpleSalary(salary_val, salary_growth_rate, Dates.Week(2), start_date, retirement_date)
    accounts = Dict{String,pf.BankAccount}()
    expenses = Dict{String,pf.RecurringExpense}()
    assets = Dict{String,pf.SimpleRetirementAsset}()

    μ = 0.10
    σ = 0.000000001
    principal = 10000.0
    asset0 = pf.SimpleRetirementAsset(Distributions.Normal(μ, σ), principal, start_date)
    assets["S&P 500"] = asset0

    rent_cost = 100.0
    expenses["Rent"] = pf.RecurringExpense(rent_cost, Dates.Month(1), start_date, 0.0)

    portfolio = pf.RetirementPortfolio(profile, salary, accounts, expenses, assets)
    out = pf.simulateRetirement(portfolio, samples=1, start_date=start_date)
    pf.plotRetirement(out, display_plots=false)
    pf.summary(portfolio, start_date=start_date, display_plots=false)

    # post-tax income, minus expenses
    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=start_date) - rent_cost * 12
    y1 = principal * (1 + μ) + val
    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=(start_date + Dates.Year(1))) - rent_cost * 12
    y2 = (y1 * (1 + μ) + val)
    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=(start_date + Dates.Year(2))) - rent_cost * 12
    y3 = (y2 * (1 + μ) + val)
    @test out.assets["S&P 500"][1] ≈ y1
    @test out.assets["S&P 500"][2] ≈ y2
    @test out.assets["S&P 500"][3] ≈ y3

    @test out.assets["S&P 500"] == out.net_worth
end
@testset "Retirement Simulation" begin
    start_date = Dates.Date(2019, 1, 1)
    birthday = Dates.Date(1950, 1, 1)
    retirement_age = 80
    life_expectancy = 80
    retirement_date = birthday + Dates.Year(retirement_age)
    profile = pf.PersonalProfile("Joe", "Joey", "Smith", birthday, retirement_age, life_expectancy)

    salary_growth_rate = 0.00
    salary_val = 10000.0
    salary = pf.SimpleSalary(salary_val, salary_growth_rate, Dates.Week(2), start_date, retirement_date)
    accounts = Dict{String,pf.BankAccount}()
    expenses = Dict{String,pf.RecurringExpense}()
    assets = Dict{String,pf.SimpleRetirementAsset}()

    accounts["Chase Checking"] = pf.BankAccount(50000.0, 0.0, Dates.Year(1))

    μ = 0.10
    σ = 0.000000001
    principal = 10000.0
    asset0 = pf.SimpleRetirementAsset(Distributions.Normal(μ, σ), principal, start_date)
    assets["S&P 500"] = asset0

    rent_cost = 100.0
    expenses["Rent"] = pf.RecurringExpense(rent_cost, Dates.Month(1), start_date, 0.0)

    e2a = Dict{String,String}()
    e2a["Rent"] = "S&P 500"

    portfolio = pf.RetirementPortfolio(profile, salary, accounts, expenses, assets, e2a)
    out = pf.simulateRetirement(portfolio, samples=1, start_date=start_date)
    pf.plotRetirement(out, display_plots=false)

    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=start_date) - rent_cost * 12
    y1 = principal * (1 + μ) + val
    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=(start_date + Dates.Year(1))) - rent_cost * 12
    y2 = (y1 * (1 + μ) + val)
    val = pf.calcAnnualSalary(salary, estimate_tax=true, date=(start_date + Dates.Year(2))) - rent_cost * 12
    y3 = (y2 * (1 + μ) + val)
    @test out.assets["S&P 500"][1] ≈ y1
    @test out.assets["S&P 500"][2] ≈ y2
    @test out.assets["S&P 500"][3] ≈ y3

    @test out.assets["S&P 500"] == out.net_worth
end