@testset "SimpleSalary" begin
    @testset "Annual & Period Calculations" begin
        start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        # 28389.55 -> 24000 after tax
        starting_salary = 28389.55
        growth_rate = 0.03
        end_date = start_date + Dates.Year(10)

        salary = pf.SimpleSalary(starting_salary, growth_rate, period, start_date, end_date)

        @test pf.calcAnnualSalary(salary, date=start_date) == starting_salary
        @test pf.calcAnnualSalary(salary, date=(end_date + Dates.Year(2))) == 0.0
        @test pf.calcPaycheck(salary, date=start_date) ≈ starting_salary / 12

        dy = 5
        nd = start_date + Dates.Year(dy)
        new_salary = pf.calcAnnualSalary(salary, date=nd)
        @test new_salary == starting_salary * (1 + growth_rate)^dy

        new_salary_taxed = pf.calcAnnualSalary(salary, date=nd, estimate_tax=true)
        @test new_salary > new_salary_taxed
    end
    @testset "Calculating paychecks as functions of time" begin
        start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        starting_salary = 24000
        growth_rate = 0.05
        salary = pf.SimpleSalary(starting_salary, growth_rate, period, start_date)

        end_date = start_date + Dates.Month(2)
        dates, paychecks = pf.getSalaryPerDay(salary, start_date, end_date)
        @test length(dates) == length(paychecks)
        @test paychecks[end] == starting_salary / 12

        start_date = Date(2019, 12, 1)
        end_date = start_date + Dates.Month(15)
        dates, paychecks = pf.getSalaryPerDay(salary, start_date, end_date)
        @test length(dates) == length(paychecks)
        @test paychecks[1] == starting_salary / 12
        @test paychecks[2] == (starting_salary * (1 + growth_rate)) / 12
        @test paychecks[end] == (starting_salary * (1 + growth_rate)^2) / 12
    end
    @testset "Calculating paychecks as functions of time, with salary end date" begin
        start_date = Dates.Date(2019, 1, 1)
        period = Dates.Month(1)
        starting_salary = 24000
        growth_rate = 0.05
        end_date = start_date + Dates.Month(17)
        salary = pf.SimpleSalary(starting_salary, growth_rate, period, start_date, end_date)

        end_date = start_date + Dates.Month(2)
        dates, paychecks = pf.getSalaryPerDay(salary, start_date, end_date)
        @test length(dates) == length(paychecks)
        @test paychecks[end] == starting_salary / 12

        start_date = Date(2019, 12, 1)
        end_date = start_date + Dates.Month(20)
        dates, paychecks = pf.getSalaryPerDay(salary, start_date, end_date)
        @test length(dates) == 7
        @test length(dates) == length(paychecks)
        @test paychecks[1] == starting_salary / 12
        @test paychecks[2] == (starting_salary * (1 + growth_rate)) / 12
    end
end
@testset "Salary Scaling" begin
    @testset "Linear saturation" begin
        xf = 5
        yf = 10
        @test pf.scale_linear_saturation(0, xf, yf) == 1.0
        @test pf.scale_linear_saturation(xf, xf, yf) == 10.0
        @test pf.scale_linear_saturation(2 * xf, xf, yf) == 10.0
        @test pf.scale_linear_saturation(.5 * xf, xf, yf) == 5.5
    end
end