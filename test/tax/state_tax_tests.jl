# Tables from: https://www.ftb.ca.gov/file/personal/tax-calculator-tables-rates.asp
# Calculations checked against: https://www.irscalculators.com/tax-calculator
# Note: the online calculator seems to have the wrong exemptions and deductions.
#       I updated it to match against what's in the official table.
@testset "California Tax: Single" begin
    @test pf.calcStateTax(100000.0, "CA", 2018) ≈ 6144.142
    @test pf.calcStateTax(200000.0, "CA", 2018) ≈ 15444.142
    @test pf.calcStateTax(473000, "CA", 2019) ≈ 43592.631
    @test pf.calcStateTax(1000, "CA", 2019) == 0.0

    @test pf.hasStateTax("CA", 2018) == true
    @test pf.hasStateTax("CA", 2019) == true
    @test pf.hasStateTax("CA", 1800) == false

    @test_throws ArgumentError pf.calcStateTax(100000.0, "CA", 1800)
    @test_throws ArgumentError pf.calcStateTax(100000.0, "WA", 2019)
end

@testset "California Tax: Married Joint" begin
    @test pf.calcStateTax(50000.0, "CA", 2021, pf.FilingStatus.married_joint) ≈ 616.22
    @test pf.calcStateTax(100000.0, "CA", 2021, pf.FilingStatus.married_joint) ≈ 2941.70
    @test pf.calcStateTax(200000.0, "CA", 2021, pf.FilingStatus.married_joint) ≈ 11687.224
    @test pf.calcStateTax(500000.0, "CA", 2021, pf.FilingStatus.married_joint) ≈ 39587.224
end