@testset "Capital Gains Tax" begin
    @testset "Capital Gains Tax: Single" begin
        @test 0.0 == pf.calcCapitalGainsTax(0.0, 2019, filing_status=pf.FilingStatus.single)
        @test 0.0 == pf.calcCapitalGainsTax(39375, 2019, filing_status=pf.FilingStatus.single)
        @test .15 * 10000 == pf.calcCapitalGainsTax(49375, 2019, filing_status=pf.FilingStatus.single)
        @test (.15 * (434550 - 39375) + .2 * (450000 - 434550)) == pf.calcCapitalGainsTax(450000, 2019, filing_status=pf.FilingStatus.single)
    end
    @testset "Capital Gains Tax: Married Joint" begin
        @test 0.0 == pf.calcCapitalGainsTax(0.0, 2019, filing_status=pf.FilingStatus.married_joint)
        @test 0.0 == pf.calcCapitalGainsTax(52750, 2019, filing_status=pf.FilingStatus.married_joint)
        @test .15 * 10000 == pf.calcCapitalGainsTax(62750, 2019, filing_status=pf.FilingStatus.married_joint)
        @test (.15 * (461700 - 52750) + .2 * (500000 - 461700)) == pf.calcCapitalGainsTax(500000, 2019, filing_status=pf.FilingStatus.married_joint)
    end
    @testset "Reverse Table Lookup" begin
        @test pf.reverseLookupCapitalGains(39375.0, 2019, filing_status=pf.FilingStatus.single) == 39375.0

        amount_wanted = 300000.0
        year = 2019
        fs = pf.FilingStatus.single
        total_needed = pf.reverseLookupCapitalGains(amount_wanted, year, filing_status=fs)
        @test (amount_wanted + pf.calcCapitalGainsTax(total_needed, year, filing_status=fs)) == total_needed

        amount_wanted = 434550.0
        total_needed = pf.reverseLookupCapitalGains(amount_wanted, year, filing_status=fs)
        @test (amount_wanted + pf.calcCapitalGainsTax(total_needed, year, filing_status=fs)) == total_needed

        amount_wanted = 500000.0
        total_needed = pf.reverseLookupCapitalGains(amount_wanted, year, filing_status=fs)
        @test (amount_wanted + pf.calcCapitalGainsTax(total_needed, year, filing_status=fs)) == total_needed
    end
end