@testset "Tax availability" begin
    state, year, filing_status = pf.getValidTaxConfiguration()
    @test pf.hasTax(state, year, filing_status) == true
end
@testset "Federal Tax Tests" begin
    include("federal_tax_tests.jl")
end
@testset "State Tax Tests" begin
    include("state_tax_tests.jl")
end
@testset "Capital Gains Tax Tests" begin
    include("capital_gains_tax_tests.jl")
end