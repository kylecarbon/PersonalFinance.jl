@testset "Pay Period" begin
    valid_pay_periods = pf.getValidPayPeriods()
    @test pf.verifyPayPeriod(Dates.Hour(1), valid_pay_periods) == false
    @test pf.verifyPayPeriod(Dates.Day(1), valid_pay_periods) == true
    @test pf.verifyPayPeriod(Dates.Week(1), valid_pay_periods) == true
    @test pf.verifyPayPeriod(Dates.Week(2), valid_pay_periods) == true
    @test pf.verifyPayPeriod(Dates.Week(4), valid_pay_periods) == true
end

@testset "Calculate Salary for Pay Period" begin
    @test pf.calcPaycheck(10000, Dates.Week(4)) ≈ 10000.0 / 13
    @test pf.calcPaycheck(10000, Dates.Week(2)) ≈ 10000.0 / 26
    @test pf.calcPaycheck(10000, Dates.Week(1)) ≈ 10000.0 / 52
    @test pf.calcPaycheck(10000, Dates.Day(1)) ≈ 10000.0 / 364
end

@testset "Calculate Pay Days" begin
    a = pf.SimpleSalary(100000, Dates.Week(2))
    a.start_date = Dates.Date(2019, 3, 1)
    end_date = Dates.Date(2019, 3, 31)
    paydays = pf.calcPayDays(a, a.start_date, end_date)
    @test length(paydays) == 3
    paydays = pf.calcPayDays(a, Dates.Date(2019, 3, 5), end_date)
    @test length(paydays) == 2
end
