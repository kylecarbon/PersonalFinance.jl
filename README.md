# PersonalFinance.jl
[![pipeline status](https://gitlab.com/kylecarbon/PersonalFinance.jl/badges/master/pipeline.svg)](https://gitlab.com/kylecarbon/PersonalFinance.jl/pipelines)
[![coverage report](https://gitlab.com/kylecarbon/PersonalFinance.jl/badges/master/coverage.svg)](https://codecov.io/gl/kylecarbon/PersonalFinance.jl/branch/master)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](https://kylecarbon.gitlab.io/PersonalFinance.jl)

A simple tool to help with budgeting and retirement planning.

# DISCLAIMER
* I am neither a qualified nor licensed investment advisor.
* All information found here is for estimation purposes only and should not be construed as personal investment advice. While the information provided is believed to be generally accurate, it may include errors or inaccuracies.
* I will not and cannot be held liable for any actions you take as a result of anything you read or use from here.
* Conduct your own due diligence, or consult a licensed financial advisor or broker before making any and all investment decisions. Any investments, trades, speculations, or decisions made on the basis of any information found on this site, expressed or implied herein, are committed at your own risk, financial or otherwise.