#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    SimReturns

Sorry for the name...Simple struct to wrap:
* `dates::Vector{Date}`
* `returns::Vector{Float64}`
"""
mutable struct SimReturns
    dates::Vector{Date}
    returns::Vector{Float64}
end

SimReturns() = SimReturns(Vector{Date}(undef, 0), Vector{Float64}(undef, 0))

"""
    simulateBudget(portfolio::SimplePortfolio; duration = Dates.Week(12), start_date = Dates.today(), state = "CA")

Simulate your assets & liabilities on a DAILY basis over the next `duration`. Returns a [`SimReturns`](@ref) as output.
This only simulates highly liquid vehicles, e.g. [`BankAccount`](@ref) and [`RecurringExpense`](@ref)s.
This is meant to help visualise your short-term cash position.
"""
function simulateBudget(portfolio::SimplePortfolio; duration=Dates.Week(12), start_date=Dates.today(), state="CA")
    if !validateExpenseToAssetMapping(portfolio.expenses2assets, keys(portfolio.expenses), keys(portfolio.accounts))
        error("Incorrect mapping of expenses to assets. Please double-check $(portfolio.expenses2assets).")
    end
    end_date = start_date + duration
    sim_dates = start_date:Dates.Day(1):end_date

    # calculate all recurring expenses and their dates of occurrence
    deltas = Vector{Pair{Vector{Date},Vector{Float64}}}()
    for (expense_name, expense) ∈ portfolio.expenses
        dates, expenses = getValuePerDay(expense, start_date, end_date)
        push!(deltas, Pair(dates, expenses))
    end

    # estimate salary, adjusted for taxes (federal, FICA, state)
    dates, paychecks = getSalaryPerDay(portfolio.salary, start_date, end_date,
        estimate_tax=true,
        state=state)
    push!(deltas, Pair(dates, paychecks))

    sim_out = SimReturns()
    sim_out.dates = sim_dates
    returns = zeros(length(sim_dates))
    returns[1] = sumAssets(portfolio)

    for (index, date) ∈ enumerate(sim_dates)
        if index == 1
            prev_value = returns[index]
        else
            prev_value = returns[index-1]
        end
        value = prev_value
        for (days, values) ∈ deltas
            for (delta_ind, day) ∈ enumerate(days)
                if day == date
                    value += values[delta_ind]
                end
            end
        end
        returns[index] = value
    end

    sim_out.returns = returns
    return sim_out
end

function simulateBudget(portfolio::RetirementPortfolio; duration=Dates.Week(12), start_date=Dates.today(), state="CA")
    new_portfolio = SimplePortfolio(portfolio.salary, portfolio.accounts, portfolio.expenses)
    return simulateBudget(new_portfolio, duration=duration, start_date=start_date, state=state)
end

"""
    RetirementSimOutput

A struct which contains:
* `portfolio`: the input [`RetirementPortfolio`](@ref) used as an input
* `start_date::Date`: the starting date for the simulation
* `salary::Vector{Float64}`: salary as a function of time
* `expenses::Dict{String, Vector{Float64}}`: each expense is tracked as a function of time
* `savings::Vector{Float64}`: expected savings as a function of time
* `assets::Dict{String,Matrix{Float64}}`: per asset, output a matrix of results, where rows correspond to number of years, columns are individual samples from the Monte Carlo simulation
* `liabilities::Dict{String,Vector{Float64}}`: per liability, the value as a function of time
* `net_worth::Matrix{Float64}`: net worth as a function of time. Rows correspond to number of years from `start_date`, whereas columns are samples from the Monte Carlo simulation.
"""
mutable struct RetirementSimOutput
    portfolio::RetirementPortfolio
    start_date::Date
    salary::Vector{Float64}
    taxes::Dict{String,Vector{Float64}}
    expenses::Dict{String,Vector{Float64}}
    savings::Vector{Float64}
    assets::Dict{String,Matrix{Float64}}
    liabilities::Dict{String,Vector{Float64}}
    net_worth::Matrix{Float64}
end

"""
    simulateRetirement(portfolio::RetirementPortfolio; start_date = Dates.today(), samples = 10000)

Run a simple Monte Carlo simulation of your [`RetirementPortfolio`](@ref) on an annual basis to help plan for your retirement.
Assumptions include:
* `samples` are used to sample from [`SimpleRetirementAsset`](@ref)
* Expenses are estimated on an annual basis via [`estimateAnnualExpenses`](@ref)
* Salary is also estimated on an annual basis (TODO: document this)
    * Taxes are calculated using the most recent tax year
* Leftover funds after accounting for expenses and taxes are saved (split equally among all retirement assets in the portfolio)
* Assets are assumed to be taxable accounts -- only long-term capital gains are currently estimated
    * To estimate long-term capital gains taxes, withdrawals are summed across all accounts, then taxed according to the capital gains tax tables

TODOs:
* allow for a mapping of savings to different retirement assets
* account for different types of retirement accounts (e.g. traditional vs. Roth accounts)
* handle all start dates correctly, e.g. if the sim start date is after an asset's start date, the asset needs to appreciate before the sim starts
* calculate capital gains tax correctly
    * sum total gains and losses over all accounts
    * consider cost averaging, FIFO, etc.
* allow withdrawing expenses from all accounts (only assets currently supported)
* support real estate investment as an asset & liability, not just the down payment and mortgage payments
"""
function simulateRetirement(portfolio::RetirementPortfolio; start_date=Dates.today(), samples=10000)
    asset_names = collect(keys(portfolio.investments))
    append!(asset_names, collect(keys(portfolio.accounts)))
    if !validateExpenseToAssetMapping(portfolio.expenses2assets, keys(portfolio.expenses), asset_names)
        error("Incorrect mapping of expenses to assets. Please double-check $(portfolio.expenses2assets).")
    end

    retirement_date = portfolio.profile.birthdate + Dates.Year(portfolio.profile.retirement_age)
    expected_death_date = portfolio.profile.birthdate + Dates.Year(portfolio.profile.life_expectancy)

    sim_date_range = start_date:Dates.Year(1):expected_death_date
    num_years = length(sim_date_range)

    # Initialise variables
    market_returns = Dict{String,Matrix{Float64}}()
    # matrix rows are years, columns are samples
    assets = Dict{String,Matrix{Float64}}()
    liabilities = Dict{String,Vector{Float64}}()
    taxes = Dict{String,Vector{Float64}}()
    expenses = Dict{String,Vector{Float64}}()
    for key ∈ keys(portfolio.expenses)
        expenses[key] = Vector{Float64}()
        sizehint!(expenses[key], num_years)
    end
    net_worth = zeros(num_years, samples)
    salary = zeros(num_years)
    taxes["Salary (Federal, FICA, State)"] = zeros(num_years)
    taxes["Capital Gains (long-term)"] = zeros(num_years)
    savings = zeros(num_years)
    for (key, investment) ∈ portfolio.investments
        market_returns[key] = rand(investment.fund, num_years, samples)
        zf = zeros(num_years, samples)
        # TODO: calculate possible return for principal based on years into the future
        # if the fund was started in 1990 but today is 2019, the principal should not be the same
        zf[1, :] .= investment.principal
        assets[key] = zf
    end
    a2e = getAssetsToExpenses(portfolio.expenses2assets)

    # Get mortgage information
    # for (house_name, val) ∈ portfolio.real_estate
    # end

    for cur_date ∈ sim_date_range
        # for iYear = 1:num_years
        iYear = year(cur_date) - year(start_date) + 1
        cur_salary = calcAnnualSalary(portfolio.salary, date=cur_date)
        salary[iYear] = cur_salary

        # TODO: deduct pre-tax contributions (if any)

        # Calculate post-tax income
        post_tax_income = calcAnnualSalary(portfolio.salary, date=cur_date, estimate_tax=true, print_debug=false)
        taxes["Salary (Federal, FICA, State)"][iYear] = cur_salary - post_tax_income

        unmapped_expenses = calculateExpenses!(expenses, portfolio.expenses, cur_date, portfolio.expenses2assets)
        # If not retired, pay for unmapped expenses via salary.
        # Otherwise, expenses are paid for out of retirement accounts.
        salaried_expenses = cur_date <= retirement_date ? unmapped_expenses : 0.0
        retirement_expenses = unmapped_expenses - salaried_expenses

        savings[iYear] = post_tax_income + salaried_expenses
        if savings[iYear] < 0.0
            error("Not enough income ($post_tax_income) to support expenses ($unmapped_expenses).")
        end

        # Calculate capital gains tax from unmapped expenses (if in retirement) and expenses
        # which are directly mapped to taxable assets
        amount_needed = retirement_expenses
        amount_needed += sumExpensesFromTaxedAssets(assets, expenses, portfolio.expenses2assets)
        withdrawal_amount = safeReverseLookupCapitalGains(-amount_needed, year(cur_date))
        cg_tax = safeCalcCapitalGainsTax(withdrawal_amount, year(cur_date))
        taxes["Capital Gains (long-term)"][iYear] = cg_tax
        # For now, assume capital gains tax and general expenses are deducted out of all accounts equally
        shared_expense = (cg_tax + retirement_expenses) / length(portfolio.investments)
        # Additionally, assume savings are contributed to each account equally
        saved_amount = savings[iYear] / length(portfolio.investments)

        # Monte-Carlo sim for asset returns
        for iSample = 1:samples
            for (key, investment) ∈ portfolio.investments
                # Propagate market returns based on previous year's amount
                market_return = market_returns[key][iYear, iSample]
                prev_return = iYear > 1 ? assets[key][iYear-1, iSample] : assets[key][iYear, iSample]
                annual_return = (1 + market_return) * prev_return

                # Add any savings or deduct any expenses
                annual_return += saved_amount
                annual_return += shared_expense
                if haskey(a2e, key)
                    for expense_name ∈ a2e[key]
                        annual_return += expenses[expense_name][iYear]
                    end
                end
                assets[key][iYear, iSample] = annual_return
                net_worth[iYear, iSample] += annual_return
            end
        end

        # Calculate net worth = assets - liabilities
        # E.g. include mortgage
        total_liabilities = sumAllLiabilitiesAnnually(portfolio.liabilities, cur_date)
        net_worth[iYear, :] .+= total_liabilities
    end
    return RetirementSimOutput(portfolio, start_date, salary, taxes, expenses, savings, assets, liabilities, net_worth)
end

function calculateExpenses!(expense_results, expenses, date, e2a)
    unmapped_expenses = 0.0
    for (key, expense) ∈ expenses
        temp_cost = estimateAnnualExpenses(expense, date)
        if key ∉ keys(e2a)
            unmapped_expenses += temp_cost
        end
        push!(expense_results[key], temp_cost)
    end
    return unmapped_expenses
end

function sumExpensesFromTaxedAssets(assets, expenses, e2a)
    total_expenses = 0.0
    asset_keys = keys(assets)
    for (ek, ak) ∈ e2a
        if ak ∈ asset_keys
            total_expenses += expenses[ek][end]
        end
    end
    return total_expenses
end

function getAssetsToExpenses(e2a)
    a2e = Dict{String,Vector{String}}()
    for (ek, ak) ∈ e2a
        if !haskey(a2e, ak)
            a2e[ak] = [ek]
        else
            push!(a2e[ak], ek)
        end
    end
    return a2e
end

function sumAllLiabilitiesAnnually(liabilities, date)
    total = 0.0
    for liability ∈ liabilities
        total += sumAnnualLiability(liability, date)
    end
    return total
end