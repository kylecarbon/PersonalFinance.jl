
#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    FixedRateMortgage(principal, down_payment, annual_rate, loan_term)

* `principal::Float64`: original listing price
* `down_payment::Float64`: down payment percentage, expressed as a decimal on [0, 1]
* `annual_rate::Float64`: nominal annual rate, expressed as a decimal on [0, 1]
* `loan_term::Dates.Month`: loan term in months
    * Optionally, you can pass in `Dates.Year`
"""
struct FixedRateMortgage
    principal::Float64
    down_payment::Float64
    annual_rate::Float64
    loan_term::Dates.Month
end

FixedRateMortgage(P::Real, d::Real, r::Real, N::Dates.Month) = FixedRateMortgage(Float64(P), Float64(d), Float64(r), N)
FixedRateMortgage(P::Real, d::Real, r::Real, N::Dates.Year) = FixedRateMortgage(Float64(P), Float64(d), Float64(r), Dates.Month(N.value * 12))

"""
    calculateFixedRateMortgagePayment(m::FixedRateMortgage)

Returns the `monthly_payment` based on a [`FixedRateMortgage`](@ref).
References:
* [Wikipedia](https://en.wikipedia.org/wiki/Mortgage_calculator)
* [Bankrate](https://www.bankrate.com/calculators/mortgages/amortization-calculator.aspx)
"""
function calculateFixedRateMortgagePayment(P, annual_rate, loan_term::Dates.Month)
    r = annual_rate / 12
    N = loan_term.value
    monthly_payment = r * P / (1 - (1 + r)^(-N))
    return monthly_payment
end

function calculateFixedRateMortgagePayment(m::FixedRateMortgage)
    return calculateFixedRateMortgagePayment(m.principal * (1 - m.down_payment), m.annual_rate, m.loan_term)
end

"""
    calculateAmountOwed(mortgage::FixedRateMortgage, t)

Calculate the amount remaining that is still owed on the mortgage at the end of month `t`.
* `mortgage`: see [`FixedRateMortgage`](@ref)
* `t::Int`: month

References:
* [Wikipedia](https://en.wikipedia.org/wiki/Mortgage_calculator)
"""
function calculateAmountOwed(mortgage::FixedRateMortgage, monthly_payment, t)
    P = mortgage.principal * (1 - mortgage.down_payment)
    r = mortgage.annual_rate / 12
    due = (1 + r)^t * P - ((1 + r)^t - 1) / r * monthly_payment
end

function calculateAmountOwed(mortgage::FixedRateMortgage, t)
    monthly_payment, _ = calculateFixedRateMortgagePayment(mortgage)
    return calculateAmountOwed(mortgage, monthly_payment, t)
end

"""
    FixedRateMortgageSchedule

A struct that represents the amortization schedule for a [`FixedRateMortgage`](@ref):
* `m::FixedRateMortgage`: the input mortgage used to generate these outputs
* `total_cost`: total cost of the mortgage over its lifetime
* `total_interest`: the total interest of the mortgage over its lifetime
* `down_payment`: the down payment
* `monthly_payment`: the monthly mortgage payment
* `principal_payments, interest_payments, balance_remaining`: the amounts per year for principal and interest payments, as well as the balance remaining
"""
struct FixedRateMortgageSchedule
    m::FixedRateMortgage
    total_cost::Float64
    total_interest::Float64
    down_payment::Float64
    monthly_payment::Float64
    principal_payments::Vector{Float64}
    interest_payments::Vector{Float64}
    balance_remaining::Vector{Float64}
end

"""
    calculateFixedRateMortgageSchedule(mortgage::FixedRateMortgage)

Calculate the amortization schedule for a [`FixedRateMortgage`](@ref).
"""
function calculateFixedRateMortgageSchedule(mortgage::FixedRateMortgage; monthly_extra=0.0)
    down_payment = mortgage.principal * mortgage.down_payment
    monthly_payment = calculateFixedRateMortgagePayment(mortgage)
    monthly_payment += monthly_extra
    total_cost = 0.0
    months = 1:mortgage.loan_term.value
    principal_payments = Vector{Float64}()
    interest_payments = Vector{Float64}()
    balance_remaining = Vector{Float64}()
    r = mortgage.annual_rate / 12
    for month ∈ months
        prev_month_balance = month == 1 ? mortgage.principal * (1 - mortgage.down_payment) : balance_remaining[month-1]
        push!(interest_payments, prev_month_balance * r)
        if prev_month_balance > monthly_payment
            principal_payment = monthly_payment - interest_payments[month]
            amount_left = prev_month_balance - principal_payment
            push!(principal_payments, principal_payment)
            push!(balance_remaining, amount_left)
            total_cost += monthly_payment
        else
            principal_payment = prev_month_balance
            total_cost += principal_payment + interest_payments[month]
            push!(principal_payments, principal_payment)
            push!(balance_remaining, 0.0)
            break
        end
    end
    total_interest = total_cost - mortgage.principal * (1 - mortgage.down_payment)
    house_total_cost = total_cost + down_payment
    return FixedRateMortgageSchedule(mortgage, house_total_cost, total_interest, down_payment, monthly_payment, principal_payments, interest_payments, balance_remaining)
end

"""
    HousePayment(m, closing_costs, property_tax, insurance, maintenance)

`m` is a [`FixedRateMortgage`](@ref). The remaining variables are all expressed as a decimal on [0, 1] and represent:
* `closing_costs::Float64`: one-time closing costs as a percentage of the mortgage's principal
* `property_tax::Float64`: annual property tax as a percentage of the mortgage's principal
* `insurance::Float64`: annual insurance as a percentage of the mortgage's principal
* `maintenance::Float64`: annual house maintenance as a percentage of the mortgage's principal
* `start_date::Date`: start date
"""
struct HousePayment
    m::FixedRateMortgage
    property_tax::Float64
    insurance::Float64
    maintenance::Float64
    start_date::Date
    closing_costs::Float64
end

HousePayment(mortgage, property_tax, insurance, maintenance) = HousePayment(mortgage, property_tax, insurance, maintenance, Dates.today(), 0.0)
"""
    HousePaymentSchedule

A struct for outputting:
* `start_date::Date`: start date of the payment schedule
* `mortgage_schedule::FixedRateMortgageSchedule`: see [`FixedRateMortgageSchedule`](@ref)
* `closing_costs`: closing costs
* `property_taxes`: monthly payment for property taxes
* `insurance_payment`: monthly insurance payment
* `fixed_cost_total`: one-time, fixed costs associated with buying the house (down payment & closing costs)
* `monthly_total`: monthly total for all housing-related payments
"""
struct HousePaymentSchedule
    start_date::Date
    mortgage_schedule::FixedRateMortgageSchedule
    closing_costs::Float64
    property_taxes::Float64
    insurance_payment::Float64
    maintenance::Float64
    total::Float64
end

"""
    calculateMonthlyPayments(h::HousePayment)

Given a [`HousePayment`](@ref), calculate monthly payments including the mortgage (see [`calculateFixedRateMortgageSchedule`](@ref)), property tax, and homeowners insurance.
"""
function calculateMonthlyPayments(h::HousePayment)
    m = calculateFixedRateMortgageSchedule(h.m)
    cc = h.m.principal * h.closing_costs
    pt = h.property_tax * h.m.principal / 12
    hi = h.insurance * h.m.principal / 12
    hm = h.m.principal * h.maintenance / 12
    total = m.monthly_payment + pt + hi + hm
    return HousePaymentSchedule(h.start_date, m, cc, pt, hi, hm, total)
end

function housePaymentToLiability(h::HousePaymentSchedule)
    date_range = h.start_date:Dates.Month(1):(h.start_date+Dates.Month(length(h.mortgage_schedule.balance_remaining) - 1))
    return SimpleLiability(collect(date_range), h.mortgage_schedule.balance_remaining)
end