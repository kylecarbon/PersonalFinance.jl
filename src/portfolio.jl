#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

# A portfolio has the following main attributes
#  - a profile of the portfolio owner
#  - a map of the assets held
#    - [key, value] = [asset_name, asset_type]
#  - career statistics
#
# A profile will contain at least
#  - owner's birthday
#  - life expectancy
#  - pretax savings rate
#  - desired retirement age
#  - current state and other tax related information
# A profile may contain
#  - desired number of kids and associated costs (college, cost-of-living)
#  - mortgage payment model
#
# Currently, I expect to write the following asset types:
#  - retirement assets
#  - stock assets
#  - bank assets
#
# Career statistics will include
#  - current salary
#  - (mu, sigma) of normal distribution of expected raises
#  - pretax savings rate
#  - desired retirement salary

"""
    SimplePortfolio(salary, accounts, expenses)
    SimplePortfolio(salary, accounts, expenses, expenses2assets)

* `salary`::[`SimpleSalary`](@ref)
* `accounts`: a dictionary of [`BankAccount`](@ref)s
* `expenses`: a dictionary of [`RecurringExpense`](@ref)s
* `expenses2assets::Dict{String,String}`: an optional mapping of expenses to the assets from which they are paid.
    * If the expense is not a key in this dictionary, then it will be paid for by salary.
"""
mutable struct SimplePortfolio
    salary::SimpleSalary
    accounts::Dict{String,BankAccount}
    expenses::Dict{String,RecurringExpense}
    expenses2assets::Dict{String,String}
end

SimplePortfolio(salary, accounts, expenses) = SimplePortfolio(salary, accounts, expenses, Dict{String,String}())

"""
    RetirementPortfolio(profile, salary, accounts, expenses, investments)
    RetirementPortfolio(profile, salary, accounts, expenses, investments, real_estate)
    RetirementPortfolio(profile, salary, accounts, expenses, investments, expenses2assets)

* `profile::`[`PersonalProfile`](@ref)
* `salary::`[`SimpleSalary`](@ref)
* `accounts`: a dictionary of [`BankAccount`](@ref)s
* `expenses`: a dictionary of [`RecurringExpense`](@ref)s
* `investments`: a dictionary of [`SimpleRetirementAsset`](@ref)s
* `liabilities`: a dictionary of [`SimpleLiability`](@ref)
* `expenses2assets::Dict{String,String}`: an optional mapping of expenses to the assets from which they are paid.
    * If the expense is not a key in this dictionary, then it will be paid for by salary.
"""
mutable struct RetirementPortfolio
    profile::PersonalProfile
    salary::SimpleSalary
    accounts::Dict{String,BankAccount}
    expenses::Dict{String,RecurringExpense}
    investments::Dict{String,SimpleRetirementAsset}
    liabilities::Dict{String,SimpleLiability}
    expenses2assets::Dict{String,String}
end

RetirementPortfolio(profile, salary, accounts, expenses, investments) = RetirementPortfolio(profile, salary, accounts, expenses, investments, Dict{String,SimpleLiability}(), Dict{String,String}())
RetirementPortfolio(profile, salary, accounts, expenses, investments, expenses2assets::Dict{String,String}) = RetirementPortfolio(profile, salary, accounts, expenses, investments, Dict{String,SimpleLiability}(), expenses2assets)
RetirementPortfolio(profile, salary, accounts, expenses, investments, liabilities::Dict{String,SimpleLiability}) = RetirementPortfolio(profile, salary, accounts, expenses, investments, liabilities, Dict{String,String}())

"""
    sumAssets(portfolio)

Return sum of assets in portfolio.
"""
function sumAssets(portfolio::SimplePortfolio)
    assets = 0
    for (_, account) ∈ portfolio.accounts
        assets += account.principal
    end
    return assets
end

function sumAnnualExpenses(portfolio::SimplePortfolio, date=Dates.today())
    sumAnnualExpenses(portfolio.expenses)
end

function sumAnnualExpenses(portfolio::RetirementPortfolio, date=Dates.today())
    sumAnnualExpenses(portfolio.expenses)
end

function validateExpenseToAssetMapping(expenses2assets, expenses, assets)
    for (key, val) ∈ expenses2assets
        if key ∉ expenses
            @debug "Incorrect mapping of expenses to assets. Expense $key cannot be found."
            return false
        end
        if val ∉ assets
            @debug "Incorrect mapping of expenses to assets. Asset $val cannot be found."
            return false
        end
    end
    return true
end