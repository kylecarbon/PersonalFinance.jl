#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

ca_married_joint_tax = TaxTable()
ca_standard_deduction_married_joint = standard_deduction_type()

ca_married_joint_tax[2022] = [0.01 20198
  0.02 47884
  0.04 75576
  0.06 104910
  0.08 132590
  0.093 677278
  0.103 812728
  0.113 1354550
  0.123 Inf]
ca_standard_deduction_married_joint[2022] = 10404 + 280

ca_married_joint_tax_2021 = [0.01 18650
  0.02 44214
  0.04 69784
  0.06 96870
  0.08 122428
  0.093 625372
  0.103 750442
  0.113 1250738
  0.123 Inf]
ca_married_joint_tax[2021] = ca_married_joint_tax_2021
ca_standard_deduction_married_joint[2021] = 9606 + 258

@warn "State taxes for married filing jointly in California are not available for 2020 or earlier. 2021 tax tables are used."
ca_married_joint_tax[2020] = ca_married_joint_tax[2021]
ca_standard_deduction_married_joint[2020] = ca_standard_deduction_married_joint[2021]

ca_married_joint_tax[2019] = ca_married_joint_tax[2021]
ca_standard_deduction_married_joint[2019] = ca_standard_deduction_married_joint[2021]

ca_married_joint_tax[2018] = ca_married_joint_tax[2021]
ca_standard_deduction_married_joint[2018] = ca_standard_deduction_married_joint[2021]

state_tax["California"][FilingStatus.married_joint] = ca_married_joint_tax
state_deduction["California"][FilingStatus.married_joint] = ca_standard_deduction_married_joint