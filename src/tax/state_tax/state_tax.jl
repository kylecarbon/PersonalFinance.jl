#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

state_tax = Dict{String,Dict{FilingStatus.filing_status_type,TaxTable}}()

standard_deduction_type = Dict{Int,Float64}
state_deduction = Dict{String,Dict{FilingStatus.filing_status_type,standard_deduction_type}}()

# Tables from: https://www.ftb.ca.gov/file/personal/tax-calculator-tables-rates.asp
state_tax["California"] = Dict{FilingStatus.filing_status_type,TaxTable}()
state_deduction["California"] = Dict{FilingStatus.filing_status_type,standard_deduction_type}()
include("ca_state_tax_single.jl")
include("ca_state_tax_married_joint.jl")
include("state_tax_aliases.jl")


"""
    hasStateTax(state::String, year::Int, filing_status=FilingStatus.filing_status_type)

* `state` can be the full state name or acronym.
* See [`filing status`](@ref FilingStatus)
"""
function hasStateTax(state, year, filing_status=FilingStatus.single)
    if !(haskey(state_tax, state) && haskey(state_deduction, state))
        return false
    end
    my_state_tax = state_tax[state]
    my_state_deduction = state_deduction[state]
    if !(haskey(my_state_tax, filing_status) && haskey(my_state_deduction, filing_status))
        return false
    end
    if !(haskey(my_state_deduction[filing_status], year) && haskey(my_state_tax[filing_status], year))
        return false
    end
    return true
end

"""
    calcStateTax(salary::Float64, state::String, year::Int, filing_status=FilingStatus.filing_status_type)

Computes tax for the given state and year. Assumes simple standard deduction applies. `state` can be the full state name or acronym.
See [`filing status`](@ref FilingStatus).
"""
function calcStateTax(salary::Float64, state::String, year::Int, filing_status=FilingStatus.single)
    if !haskey(state_tax, state)
        throw(ArgumentError("PersonalFinance's state tax bracket doesn't contain the state $state."))
    end
    if !haskey(state_deduction, state)
        throw(ArgumentError("PersonalFinance's standard deduction table for states doesn't contain the state $state."))
    end
    my_state_tax = state_tax[state][filing_status]
    my_state_deduction = state_deduction[state][filing_status]
    if !haskey(my_state_tax, year)
        throw(ArgumentError("PersonalFinance's $state tax bracket doesn't contain the year $year."))
    end
    if !haskey(my_state_deduction, year)
        throw(ArgumentError("PersonalFinance's $state tax deduction doesn't contain the year $year."))
    end
    tax_table = my_state_tax[year]
    taxable_income = salary - my_state_deduction[year]
    if taxable_income <= 0.0
        return 0.0
    end
    return lookupTaxTable(taxable_income, tax_table)
end

calcStateTax(salary::Real, state::String, year::Int) = calcStateTax(Float64(salary), state, year)
