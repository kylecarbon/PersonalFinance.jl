#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

ca_single_tax = TaxTable()
ca_standard_deduction_single = standard_deduction_type()

ca_single_tax[2022] = [0.01 10099
  0.02 23942
  0.04 37788
  0.06 52455
  0.08 66295
  0.093 338639
  0.103 406364
  0.113 677275
  0.123 Inf]
ca_standard_deduction_single[2022] = 5202 + 140

ca_single_tax_2021 = [0.01 9325
  0.02 22107
  0.04 34892
  0.06 48435
  0.08 61214
  0.093 312686
  0.103 375221
  0.113 625369
  0.123 Inf]
ca_single_tax[2021] = ca_single_tax_2021
ca_standard_deduction_single[2021] = 4803 + 129

ca_single_tax_2020 = [0.01 8932
  0.02 21175
  0.04 33421
  0.06 46394
  0.08 58634
  0.093 299508
  0.103 359407
  0.113 599013
  0.123 Inf]
ca_single_tax[2020] = ca_single_tax_2020
ca_standard_deduction_single[2020] = 4601 + 124

ca_single_tax_2019 = [0.01 8809
  0.02 20883
  0.04 32960
  0.06 45753
  0.08 57824
  0.093 295373
  0.103 354445
  0.113 590742
  0.123 Inf]
ca_single_tax[2019] = ca_single_tax_2019
ca_standard_deduction_single[2019] = 4537 + 122

ca_single_tax_2018 = [0.01 8544
  0.02 20255
  0.04 31969
  0.06 44377
  0.08 56085
  0.093 286492
  0.103 343788
  0.113 572980
  0.123 Inf]
ca_single_tax[2018] = ca_single_tax_2018
ca_standard_deduction_single[2018] = 4401

state_tax["California"][FilingStatus.single] = ca_single_tax
state_deduction["California"][FilingStatus.single] = ca_standard_deduction_single