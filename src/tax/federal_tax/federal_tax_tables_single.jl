#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

federal_single_tax = TaxTable()
federal_tax[FilingStatus.single] = federal_single_tax

single_standard_deduction = DeductionTable()
deduction_dict[FilingStatus.single] = single_standard_deduction

single_tax_2023 = [0.10 11000
  0.12 44725
  0.22 95375
  0.24 182100
  0.32 231250
  0.35 578125
  0.37 Inf]
federal_single_tax[2023] = single_tax_2023
single_standard_deduction[2023] = 13850

single_tax_2022 = [0.10 10275
  0.12 41775
  0.22 89075
  0.24 170050
  0.32 215950
  0.35 539900
  0.37 Inf]
federal_single_tax[2022] = single_tax_2022
single_standard_deduction[2022] = 12950

single_tax_2021 = [0.10 9950
  0.12 40525
  0.22 86375
  0.24 164925
  0.32 209425
  0.35 523600
  0.37 Inf]
federal_single_tax[2021] = single_tax_2021
single_standard_deduction[2021] = 12550

single_tax_2020 = [0.10 9875
  0.12 40125
  0.22 85525
  0.24 163300
  0.32 207350
  0.35 518400
  0.37 Inf]
federal_single_tax[2020] = single_tax_2020
single_standard_deduction[2020] = 12400

single_tax_2019 = [0.10 9700
  0.12 39475
  0.22 84200
  0.24 160725
  0.32 204100
  0.35 510300
  0.37 Inf]
federal_single_tax[2019] = single_tax_2019
single_standard_deduction[2019] = 12200

single_tax_2018 = [0.10 9525
  0.12 38700
  0.22 82500
  0.24 157500
  0.32 200000
  0.35 500000
  0.37 Inf]
federal_single_tax[2018] = single_tax_2018
single_standard_deduction[2018] = 12000
