#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

federal_married_joint_tax = TaxTable()
federal_tax[FilingStatus.married_joint] = federal_married_joint_tax
married_joint_standard_deduction = DeductionTable()
deduction_dict[FilingStatus.married_joint] = married_joint_standard_deduction

married_joint_tax_2023 = [0.10 22000
  0.12 89450
  0.22 190750
  0.24 364200
  0.32 462500
  0.35 693750
  0.37 Inf]
federal_married_joint_tax[2023] = married_joint_tax_2023
married_joint_standard_deduction[2023] = 27700

married_tax_2022 = [0.10 20550
  0.12 83550
  0.22 178150
  0.24 340100
  0.32 431900
  0.35 647850
  0.37 Inf]
federal_married_joint_tax[2022] = married_tax_2022
married_joint_standard_deduction[2022] = 25900

married_tax_2021 = [0.10 19900
  0.12 81050
  0.22 172750
  0.24 329850
  0.32 418850
  0.35 628300
  0.37 Inf]
federal_married_joint_tax[2021] = married_tax_2021
married_joint_standard_deduction[2021] = 25100

@warn "Federal taxes for married filing jointly are not correct for 2020 and earlier. They use the tax tables for 2021. PRs are welcome to add the additional tax tables."
federal_married_joint_tax[2020] = married_tax_2021
married_joint_standard_deduction[2020] = 12400

federal_married_joint_tax[2019] = married_tax_2021
married_joint_standard_deduction[2019] = 12200

federal_married_joint_tax[2018] = married_tax_2021
married_joint_standard_deduction[2018] = 12000
