#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

include("federal_tax_tables.jl")
include("fica_tax.jl")

function calcFederalTax(salary::SimpleSalary, year::Int)
    return calcFederalTax(salary.annual_salary, year)
end

"""
    hasFederalTax(year::Int, filing_status::Int)

Returns `true` if this package contains federal tax brackets and standard deductions for the given year and [`filing status`](@ref FilingStatus)
"""
function hasFederalTax(year, filing_status=FilingStatus.single)
    return haskey(federal_tax, filing_status) && haskey(federal_tax[filing_status], year) &&
           haskey(deduction_dict, filing_status) && haskey(deduction_dict[filing_status], year)
end

"""
    calcFederalTax(salary::Float64, year::Int, filing_status::Int)

Calculates federal tax for the salary in the given year, assuming:
    * status is single
    * no dependents
    * standard deduction is applied
"""
function calcFederalTax(salary::Float64, year::Int, filing_status::Int=FilingStatus.single)
    if !hasFederalTax(year, filing_status)
        throw(ArgumentError("PersonalFinance's federal tax calculation doesn't support the year $year and/or the filing status of $filing_status."))
    end

    tax_table = federal_tax[filing_status][year]
    taxable_income = salary - deduction_dict[filing_status][year]
    if taxable_income <= 0.0
        return 0.0
    end
    return lookupTaxTable(taxable_income, tax_table)
end
calcFederalTax(salary::Real, year::Int) = calcFederalTax(Float64(salary), year)


"""
    calcSSTax(salary::Float64, year::Int)

Calculates social security tax.
"""
function calcSSTax(salary::Float64, year::Int)
    if !haskey(ss_tax, year)
        throw(ArgumentError("PersonalFinance's social security table doesn't contain the year $year."))
    end

    if salary < ss_tax[year]["limit"]
        tax_owed = salary * ss_tax[year]["rate"]
    else
        tax_owed = ss_tax[year]["limit"] * ss_tax[year]["rate"]
    end

    return tax_owed
end
calcSSTax(salary::Real, year::Int) = calcSSTax(Float64(salary), year)

"""
    calcMedicareTax(salary::Float64, year::Int)

Calculates medicare tax.
"""
function calcMedicareTax(salary::Float64, year::Int)
    if !haskey(medicare_tax, year)
        throw(ArgumentError("PersonalFinance's medicare tax table doesn't contain the year $year."))
    end
    base_tax = salary * medicare_tax[year]["base_rate"]
    if salary <= medicare_tax[year]["limit"]
        return base_tax
    end
    return base_tax + (salary - medicare_tax[year]["limit"]) * medicare_tax[year]["excess_rate"]
end
calcMedicareTax(salary::Real, year::Int) = calcMedicareTax(Float64(salary), year)

"""
    hasFICATax(year::Int)
"""
hasFICATax(year) = haskey(ss_tax, year) && haskey(medicare_tax, year)

"""
    calcFICATax(salary::Real, year::Int)

Calculates FICA tax, e.g. social security & medicare.
"""
function calcFICATax(salary::Real, year::Int)
    return calcSSTax(salary, year) + calcMedicareTax(salary, year)
end
