#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

ss_tax = Dict{Int,Dict{String,Float64}}()
ss_tax[2023] = Dict{String,Float64}()
ss_tax[2023]["rate"] = 0.062
ss_tax[2023]["limit"] = 160200
ss_tax[2022] = Dict{String,Float64}()
ss_tax[2022]["rate"] = 0.062
ss_tax[2022]["limit"] = 147000
ss_tax[2021] = Dict{String,Float64}()
ss_tax[2021]["rate"] = 0.062
ss_tax[2021]["limit"] = 142800
ss_tax[2020] = Dict{String,Float64}()
ss_tax[2020]["rate"] = 0.062
ss_tax[2020]["limit"] = 137700
ss_tax[2019] = Dict{String,Float64}()
ss_tax[2019]["rate"] = 0.062
ss_tax[2019]["limit"] = 132900

medicare_tax = Dict{Int,Dict{String,Float64}}()
medicare_tax[2019] = Dict{String,Float64}()
medicare_tax[2019]["base_rate"] = 0.0145
medicare_tax[2019]["limit"] = 200000
medicare_tax[2019]["excess_rate"] = 0.009
medicare_tax[2020] = Dict{String,Float64}()
medicare_tax[2020]["base_rate"] = 0.0145
medicare_tax[2020]["limit"] = 200000
medicare_tax[2020]["excess_rate"] = 0.009
medicare_tax[2021] = Dict{String,Float64}()
medicare_tax[2021]["base_rate"] = 0.0145
medicare_tax[2021]["limit"] = 200000
medicare_tax[2021]["excess_rate"] = 0.009
medicare_tax[2022] = Dict{String,Float64}()
medicare_tax[2022]["base_rate"] = 0.0145
medicare_tax[2022]["limit"] = 200000
medicare_tax[2022]["excess_rate"] = 0.009
medicare_tax[2023] = Dict{String,Float64}()
medicare_tax[2023]["base_rate"] = 0.0145
medicare_tax[2023]["limit"] = 200000
medicare_tax[2023]["excess_rate"] = 0.009