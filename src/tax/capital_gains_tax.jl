capital_gains_tax = Dict{FilingStatus.filing_status_type,TaxTable}()

cg_single_tax = TaxTable()
capital_gains_tax[FilingStatus.single] = cg_single_tax
cg_single_2019 = [0.0 39375.0
    0.15 434550.0
    0.2 Inf]
cg_single_tax[2019] = cg_single_2019

cg_married_joint = TaxTable()
capital_gains_tax[FilingStatus.married_joint] = cg_married_joint
cg_married_joint_2019 = [0.0 52750
    0.15 461700
    0.2 Inf]
cg_married_joint[2019] = cg_married_joint_2019

function calcCapitalGainsTax(income::Float64, year::Int; filing_status = FilingStatus.single)
    if income < 0.0
        error("Income must be greater than or equal to zero. Instead, input was: $income.")
    end
    if !hasCapitalGainsTax(year, filing_status)
        throw(ArgumentError("No capital gains tax table for $year with filing status $filing_status."))
    end
    tax_table = capital_gains_tax[filing_status][year]
    return lookupTaxTable(income, tax_table)
end

calcCapitalGainsTax(income::Real, year::Int; filing_status = FilingStatus.single) = calcCapitalGainsTax(Float64(income), year, filing_status = filing_status)

function hasCapitalGainsTax(year, filing_status)
    return haskey(capital_gains_tax, filing_status) && haskey(capital_gains_tax[filing_status], year)
end

function reverseLookupCapitalGains(amount::Float64, year::Int; filing_status = FilingStatus.single)
    if amount < 0.0
        error("Amount must be greater than or equal to zero. Instead, input was: $amount.")
    end
    if !hasCapitalGainsTax(year, filing_status)
        throw(ArgumentError("No capital gains tax table for $year with filing status $filing_status."))
    end
    tax_table = capital_gains_tax[filing_status][year]
    return reverseLookupTable(amount, tax_table)
end

function getValidCapitalGainsTaxConfiguration()
    return 2019, FilingStatus.single
end

function safeCalcCapitalGainsTax(income::Float64, year::Int; filing_status = FilingStatus.single)
    if income < 0.0
        @error "Income must be greater than or equal to zero. Instead, input was: $income."
        return 0
    end
    if !hasCapitalGainsTax(year, filing_status)
        @debug "Incomplete capital gains tax information for $year with filing status $filing_status -- update this function."
        year, filing_status = getValidCapitalGainsTaxConfiguration()
    end
    tax_table = capital_gains_tax[filing_status][year]
    return lookupTaxTable(income, tax_table)
end


function safeReverseLookupCapitalGains(amount::Float64, year::Int; filing_status = FilingStatus.single)
    if amount < 0.0
        @error "Amount must be greater than or equal to zero. Instead, input was: $amount."
        return 0
    end
    if !hasCapitalGainsTax(year, filing_status)
        @debug "Incomplete capital gains tax information for $year with filing status $filing_status -- update this function."
        year, filing_status = getValidCapitalGainsTaxConfiguration()
    end
    tax_table = capital_gains_tax[filing_status][year]
    return reverseLookupTable(amount, tax_table)
end

