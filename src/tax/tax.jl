#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    FilingStatus

Enum for filing status:
* single = 1
* married_joint = 2
* married_separate = 3
* head_of_household = 4
* widow = 5
"""
module FilingStatus
filing_status_type = Int
single = 1
married_joint = 2
married_separate = 3
head_of_household = 4
widow = 5
end

# keys are Years, represented by Ints
# values are matrices, where:
#   * 1st column: tax rate
#   * 2nd column: tax bracket value
TaxTable = Dict{Int,Matrix{Float64}}

function lookupTaxTable(income, tax_table)
    num_rows, _ = size(tax_table)
    tax_owed = 0.0
    for i = 1:num_rows
        rate = tax_table[i, 1]
        limit = tax_table[i, 2]
        lower_limit = i == 1 ? 0.0 : tax_table[i-1, 2]
        if income <= limit
            tax_owed += rate * (income - lower_limit)
            break
        elseif income > limit
            tax_owed += rate * (limit - lower_limit)
        end
    end
    return tax_owed
end

function reverseLookupTable(amount, tax_table)
    tax_owed = 0.0
    for i = 1:size(tax_table, 1)
        rate = tax_table[i, 1]
        limit = tax_table[i, 2]
        lower_limit = i == 1 ? 0.0 : tax_table[i-1, 2]

        total = (amount + tax_owed - rate * lower_limit) / (1.0 - rate)
        if total < limit
            return total
        else
            tax_owed += rate * (limit - lower_limit)
        end
    end
    return amount + tax_owed
end

############
# INCLUDES #
############
include("federal_tax/federal_tax.jl")
include("state_tax/state_tax.jl")
include("capital_gains_tax.jl")

############
# INCLUDES #
############

"""
    hasTax(state, year, filing_status)

Helper function to check if PersonalFinance has enough information to calculate taxes for the state & year provided.
See [`filing status`](@ref FilingStatus).
"""
hasTax(state, year, filing_status=FilingStatus.single) = hasFederalTax(year, filing_status) && hasFICATax(year) && hasStateTax(state, year, filing_status)

"""
    getValidTaxConfiguration

Helper function which returns a valid tax configuration.
"""
function getValidTaxConfiguration()
    state = "CA"
    year = 2020
    filing_status = FilingStatus.single
    @assert hasTax(state, year, filing_status)
    return state, year, filing_status
end

"""
    calcTax(salary, state, year)

`salary::Float64`: dollar amount of salary
`state::String`: full state name or acronym
`year::Int`: year of tax calculation

Helper function to calculate tax for a single taxpayer, which includes
federal tax, FICA tax, and state tax. Assumptions include:
    * status is single
    * no dependents
    * using standard deduction
    * any other simplifying assumption you can think of
"""
function calcTax(salary::Float64, state::String, year::Int, filing_status=FilingStatus.single)
    tax_owed = calcFederalTax(salary, year, filing_status)
    tax_owed += calcFICATax(salary, year)
    tax_owed += calcStateTax(salary, state, year)
    return tax_owed
end
function calcTax(salaries::Vector{Float64}, state::String, year::Int, filing_status=FilingStatus.married_joint)
    total_salary = 0.0
    tax_owed = 0.0
    for salary ∈ salaries
        total_salary += salary
        tax_owed += calcFICATax(salary, year)
    end
    tax_owed += calcStateTax(total_salary, state, year, filing_status)
    tax_owed += calcFederalTax(total_salary, year, filing_status)
end