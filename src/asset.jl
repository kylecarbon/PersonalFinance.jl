#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#


"""
    BankAccount(principal, rate, compound_period)

* `principal::Float64`: starting value in the account
* `rate::Float64`: annual nominal savings rate
* `compound_period::Dates.Period`: how often the account compounds

Based on the simple compound interest equation:
```math
f(t) = P\\left(1+\\frac{r}{n} \\right)^{nt}
```
"""
mutable struct BankAccount <: AbstractAsset
    principal::Float64
    rate::Float64
    compound_period::Dates.Period
    function BankAccount(principal, rate, compound_period)
        if principal < 0
            throw(DomainError(principal, "must be non-negative"))
        end
        if rate < 0
            throw(DomainError(rate, "must be non-negative"))
        end
        return new(principal, rate, compound_period)
    end
end

"""
    SimpleRetirementAsset(fund, principal, start_date, end_date)

* `fund::Distributions.ContinuousUnivariateDistribution`: the underlying distribution for the asset.
* `principal`: starting amount of the asset
* `start_date::Date`: the asset's creation date
* `end_date::Union{Date,Nothing}`: an optional end date for the asset, e.g. when it will be liquidated

SimpleRetirementAsset's are assumed to compound annually.

*Note:* this and [`BankAccount`](@ref) should eventually implement the same interface -- only meaningful
difference is that retirement asset returns are stochastic.
"""
mutable struct SimpleRetirementAsset <: AbstractRetirementAsset
    fund::Distributions.ContinuousUnivariateDistribution
    principal::Float64
    # if applicable, start and end dates
    start_date::Date
    end_date::Union{Date,Nothing}
end

SimpleRetirementAsset(fund, principal, start) = SimpleRetirementAsset(fund, principal, start, nothing)

function getValuePerDay(asset::SimpleRetirementAsset, start_date::Date, end_date::Date)
    asset_end_date = isnothing(asset.end_date) ? end_date : min(asset.end_date, end_date)

    # For a retirement asset, assume it compounds every year
    period = Dates.Year(1)
    dates = asset.start_date:period:asset_end_date
    returns = rand(asset.fund, length(dates))
    value = asset.principal
    assets = zeros(length(dates))
    for (index, date) in enumerate(dates)
        if index > 1
            value *= (1 + returns[index])
        end
        assets[index] = value
    end
    assets = assets[dates.>=start_date]
    dates = dates[dates.>=start_date]
    return dates, assets
end

"""
    SimpleLiability(dates, balance)

* `dates::Vector{Dates.Date}`
* `balance::Vector{Float64}`

A struct representing a "simple liability" of dates and the balance due at each date.
"""
mutable struct SimpleLiability
    dates::Vector{Dates.Date}
    balance::Vector{Float64}
end

function getValuePerDay(liability::SimpleLiability, start_date::Date, end_date::Date)
    tmp_dates = liability.dates
    dates = tmp_dates[start_date.<=tmp_dates.<=end_date]
    balance = balances[start_date.<=tmp_dates.<=end_date]
    return dates, balance
end

function sumAnnualLiability(liability::SimpleLiability, date::Date)
    end_date = date + Dates.Year(1) - Dates.Day(1)
    dates, balance = getValuePerDay(liability, date, end_date)
    return -sum(balance)
end