#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

# abstract type definition for salary struct
abstract type AbstractSalary end

"""
    SimpleSalary(annual_salary, pay_period)
    SimpleSalary(annual_salary, annual_growth_rate)
    SimpleSalary(annual_salary, annual_growth_rate, pay_period, start_date)

* `annual_salary::Float64`: nominal annual salary
* `annual_growth_rate::Float64`: nominal annual growth rate, expressed as a decimal on [0, 1]. Default is 0.03.
* `pay_period::Dates.Period`: how often you get paid. Default is `Dates.Week(2)`.
* `start_date::Date`: when you started receiving your current `annual_salary`
"""
mutable struct SimpleSalary
    annual_salary::Float64
    annual_growth_rate::Float64
    pay_period::Dates.Period
    start_date::Date
    end_date::Union{Date,Nothing}

    function SimpleSalary(annual_salary, growth_rate, pay_period, start_date, end_date)
        if annual_salary < 0
            throw(DomainError(annual_salary, "argument must be non-negative"))
        end
        if isnothing(pay_period)
            pay_period = Dates.Week(2)
        end
        valid_pay_periods = getValidPayPeriods()
        if !verifyPayPeriod(pay_period, valid_pay_periods)
            throw(DomainError(pay_period, "Pay period of $pay_period not found in valid set: $valid_pay_periods"))
        end
        if isnothing(start_date)
            start_date = Date(year(Dates.today()), 1, 1)
        end
        return new(annual_salary, growth_rate, pay_period, start_date, end_date)
    end
end

SimpleSalary(annual_salary::Real, pay_period::Dates.Period) = SimpleSalary(Float64(annual_salary), 0.03, pay_period, nothing, nothing)
SimpleSalary(annual_salary::Real, growth_rate::Real) = SimpleSalary(Float64(annual_salary), Float64(growth_rate), Dates.Week(2), nothing, nothing)
SimpleSalary(annual_salary::Real, growth_rate::Real, pay_period::Dates.Period, start_date::Date) = SimpleSalary(Float64(annual_salary), Float64(growth_rate), pay_period, start_date, nothing)

function getValidPayPeriods()
    allowed_pay_periods = Set{Dates.Period}()

    push!(allowed_pay_periods, Dates.Day(1))
    push!(allowed_pay_periods, Dates.Week(1))
    push!(allowed_pay_periods, Dates.Week(2))
    push!(allowed_pay_periods, Dates.Week(4))
    push!(allowed_pay_periods, Dates.Month(1))

    return allowed_pay_periods
end

function verifyPayPeriod(pay_period::Dates.Period, valid_pay_periods)
    return pay_period ∈ valid_pay_periods
end

function calcPaycheck(annual_salary::Real, pay_period::Dates.Period)
    return annual_salary / calcNumPeriodsPerYear(pay_period)
end

"""
      ___
     /
    /
___/

Return a scalar based on:
* x: time from start time
* xf: final time at which scalar levels off
* yf: final scalar value

Assumes (x_0, y_0) = (0, 1).
"""
function scale_linear_saturation(x, xf, yf)
    x0 = 0
    y0 = 1
    dy = yf - y0
    dx = xf - x0
    k = dy / dx
    if x < x0
        return 1
    elseif x > xf
        return yf
    else
        return k * x + y0
    end
end

"""
    calcAnnualSalary(salary::SimpleSalary;
                     estimate_tax = false,
                     state = "CA",
                     date = Dates.today())

Calculate your annual salary on `date`, assuming [`SimpleSalary`](@ref)'s growth model. Optionally, estimate taxes.
"""
function calcAnnualSalary(salary::SimpleSalary;
    estimate_tax=false,
    state="CA",
    date=Dates.today(),
    print_debug=true,
    salary_scale="")
    if (date < salary.start_date) || (!isnothing(salary.end_date) && date > salary.end_date)
        return 0.0
    end
    delta_years = year(date) - year(salary.start_date)
    if salary_scale == "linear"
        current_salary = salary.annual_salary * scale_linear_saturation(delta_years, 20, 1.67)
    else
        current_salary = salary.annual_salary * (1 + salary.annual_growth_rate)^delta_years
    end

    if !estimate_tax
        return current_salary
    end
    if !hasTax(state, year(date))
        new_state, new_year = getValidTaxConfiguration()
        new_date = Date(new_year, 1, 1)
        if print_debug
            @warn "Incomplete tax information for $date in $state -- defaulting to $new_date in $new_state."
        end
        state = new_state
        date = new_date
    end

    annual_tax = calcTax(current_salary, state, year(date))
    return current_salary - annual_tax
end

"""
    calcPaycheck(salary::SimpleSalary;
                 estimate_tax=false,
                 state="CA",
                 date=Dates.today())

Calculate your paycheck for the pay period given your [`SimpleSalary`](@ref).
* `estimate_tax`: optionally, estimate taxes when calculating your take-home paycheck
* `state`: optionally, provide the state for tax estimation
* `date`: optionally, provide a date for when to estimate your pay check
"""
function calcPaycheck(salary::SimpleSalary;
    estimate_tax=false,
    state="CA",
    date=Dates.today())
    annual_salary = calcAnnualSalary(salary, estimate_tax=estimate_tax, state=state, date=date)
    return annual_salary / calcNumPeriodsPerYear(salary.pay_period)
end

function calcPayDays(salary::SimpleSalary, start_date::Date, end_date::Date)
    if !isnothing(salary.end_date)
        end_date = min(salary.end_date, end_date)
    end
    return calcDaysOfOcurrence(salary.pay_period, salary.start_date, start_date, end_date)
end

function calcPayDays(salary::SimpleSalary, end_date::Date)
    return calcPayDays(salary, Dates.today(), end_date)
end

"""
    getSalaryPerDay(salary::SimpleSalary, start_date::Date, end_date::Date;
                   estimate_tax = false,
                   state = "CA")

Given a [`SimpleSalary`](@ref), return a tuple of `(dates, paychecks)`
1. `dates`: a vector of dates on which you will receive paychecks
1. `paychecks`: a vector of values of each paycheck

The paycheck calculation can optionally be adjusted based on estimated taxes.
"""
function getSalaryPerDay(salary::SimpleSalary, start_date::Date, end_date::Date;
    estimate_tax=false,
    state="CA")
    ssdy = Dates.year(salary.start_date)
    sdy = Dates.year(start_date)
    dates = Vector{Dates.Date}()
    paychecks = Vector{Float64}()
    if (sdy < ssdy)
        @warn "Salary start date ($(salary.start_date)) must be before this calculation's start date ($start_date)."
        return dates, paychecks
    end
    dates = calcPayDays(salary, start_date, end_date)

    current_year = sdy
    current_paycheck = calcPaycheck(salary; estimate_tax=estimate_tax, state=state, date=start_date)

    for date in dates
        new_year = Dates.year(date)
        if new_year != current_year
            current_year = new_year
            current_paycheck = calcPaycheck(salary; estimate_tax=estimate_tax, state=state, date=date)
        end
        push!(paychecks, current_paycheck)
    end
    @assert length(paychecks) == length(dates) "Length of paychecks ($(length(paychecks))) must match length of dates ($(length(dates)))."
    return dates, paychecks
end
