#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

"""
    plotRetirement(simresults::RetirementSimOutput; display_plots=true)

Given a [`RetirementSimOutput`](@ref), this helper function returns a `Dict{String,Plot}`,
containing several plots to visualise returns of Monte Carlo simulations of fund performance.
"""
function plotRetirement(simresults::RetirementSimOutput; quantiles=[0.25 0.5 0.75], display_plots=true)
    @assert length(quantiles) == 3 "Quantiles must have length 3"
    assets = simresults.assets
    profile = simresults.portfolio.profile

    vals = collect(values(assets))
    num_years, num_samples = size(vals[1])
    returns = zeros(num_years, num_samples)
    for (asset_name, asset_returns) ∈ assets
        returns += asset_returns
    end

    assets_plot_time = plotSampledReturnsVersusTime(returns, "Total Assets", simresults.start_date, quantiles=quantiles)
    assets_histogram = plotDistribution(returns[end, :], "Total Assets")
    income_plot, expenses_plot_monthly, expenses_plot_annual = plotSalaryExpenses(simresults)
    d = plotSampledReturnsVersusTime(simresults.net_worth, "Net Worth", simresults.start_date, quantiles=quantiles)
    e = plotDistribution(simresults.net_worth[end, :], "Net Worth")
    f = plotpie(averageRecurringExpensesOverOneYear(simresults.portfolio.expenses, simresults.start_date), "Average Recurring Expenses per Month (US\$) on $(simresults.start_date)")
    plots = Dict{String,PlotlyJS.SyncPlot}()
    plots["Total Assets"] = [assets_plot_time assets_histogram]
    plots["Income Plot"] = income_plot
    plots["Estimated Monthly Expenses"] = expenses_plot_monthly
    plots["Estimated Annual Expenses"] = expenses_plot_annual
    plots["Net Worth"] = [d e]
    plots["Expenses Pie Plot"] = f
    if display_plots
        display(plots["Total Assets"])
        display(plots["Income Plot"])
        display(plots["Estimated Monthly Expenses"])
        display(plots["Estimated Annual Expenses"])
        display(plots["Net Worth"])
        display(plots["Expenses Pie Plot"])
    end
    return plots
end

function plotSampledReturnsVersusTime(returns::Matrix{Float64}, plot_title, start_date; quantiles=[0.25 0.5 0.75])
    @assert length(quantiles) == 3 "Quantiles must have length 3"
    num_years, num_samples = size(returns)

    low = zeros(num_years)
    mid = zeros(num_years)
    high = zeros(num_years)
    for iYear = 1:num_years
        annual_return = returns[iYear, :]
        # normal_fit = Distributions.fit(Distributions.Normal, annual_return)
        quantile_fit = Distributions.quantile(annual_return, quantiles)
        low[iYear] = quantile_fit[1]
        mid[iYear] = quantile_fit[2]
        high[iYear] = quantile_fit[3]
    end
    date_year = collect(1:num_years) .- 1 .+ Dates.year(start_date)

    trace1 = PlotlyJS.scatter(; x=date_year,
        y=mid,
        name="Median",
        marker=PlotlyJS.attr(color="black",
            line_color="black",
            line_width=2,
            size=10,
            symbol="circle"),
        mode="lines+markers")
    trace2 = PlotlyJS.scatter(; x=date_year,
        y=low,
        name="Median ± 25%",
        marker=PlotlyJS.attr(color="grey",
            line_color="grey",
            line_width=1,))
    trace3 = PlotlyJS.scatter(; x=date_year,
        y=high,
        fill="tonexty",
        name="Median ± 25%",
        showlegend=false,
        marker=PlotlyJS.attr(color="grey",
            line_color="grey",
            line_width=1,))

    layout_assets_vs_time = PlotlyJS.Layout(; title="$plot_title vs. year",
        xaxis=PlotlyJS.attr(title="Year"),
        yaxis=PlotlyJS.attr(title="$plot_title (US\$)"))

    assets_vs_time = [trace2, trace3, trace1]

    p1 = PlotlyJS.plot(assets_vs_time, layout_assets_vs_time)
    return p1
end

function plotDistribution(returns, plot_title; xlim=[0 0.95])
    trace_hist1 = PlotlyJS.histogram(x=returns,
        histnorm="probability",
        marker=PlotlyJS.attr(color="#1f77b4"),
        showlegend=false)

    xlim_val = Distributions.quantile(returns, xlim)
    layout_hist = PlotlyJS.Layout(; title="Distribution of $plot_title",
        xaxis=PlotlyJS.attr(title="$plot_title (US\$)",
            range=[xlim_val[1], xlim_val[2]]),
        yaxis=PlotlyJS.attr(title="Probability"))
    trace_hist = [trace_hist1]
    p2 = PlotlyJS.plot(trace_hist1, layout_hist)
    return p2
end

function plot(returns::SimReturns)
    trace1 = PlotlyJS.scatter(; x=returns.dates,
        y=returns.returns,
        mode="lines+markers")
    layout = PlotlyJS.Layout(; title="Funds vs. time",
        xaxis=PlotlyJS.attr(title="Date"),
        yaxis=PlotlyJS.attr(title="Funds (US\$)"))
    PlotlyJS.plot([trace1], layout)
end

function plotpie(x, y, plot_title)
    trace = PlotlyJS.pie(; labels=x,
        values=y,
        textinfo="label+percent+value")
    layout = PlotlyJS.Layout(; title=plot_title)
    PlotlyJS.plot([trace], layout)
end

function plotpie(expenses::Dict{String,Float64}, plot_title)
    x = Vector{String}()
    y = Vector{Float64}()
    monthly_total = 0
    for (key, val) ∈ expenses
        push!(x, key)
        push!(y, val)
        monthly_total += val
    end
    new_title = plot_title * ": \$$(round(monthly_total, digits=2))"
    return plotpie(x, y, new_title)
end

function plotSalaryExpenses(simresults)

    start_date = simresults.start_date
    salary = simresults.salary
    expenses = simresults.expenses
    savings = simresults.savings
    taxes = simresults.taxes["Salary (Federal, FICA, State)"]

    num_years = length(salary)
    date_year = collect(1:num_years) .+ Dates.year(start_date) .- 1
    salary_trace = PlotlyJS.scatter(; x=date_year,
        y=salary,
        name="Salary",
        mode="lines+markers")
    traces = [salary_trace]
    expense_traces_monthly = Vector{PlotlyJS.GenericTrace}()
    expense_traces_annual = Vector{PlotlyJS.GenericTrace}()
    total_expenses = zeros(num_years)
    for (expense_name, expense) ∈ expenses
        total_expenses += expense
        tmp_trace = PlotlyJS.scatter(; x=date_year,
            y=-expense / 12,
            name=expense_name,
            mode="lines+markers")
        push!(expense_traces_monthly, tmp_trace)
        tmp_trace1 = PlotlyJS.scatter(; x=date_year,
            y=-expense,
            name=expense_name,
            mode="lines+markers")
        push!(expense_traces_annual, tmp_trace1)
    end
    total_expense_trace = PlotlyJS.scatter(; x=date_year,
        y=-total_expenses,
        name="Total Living Expenses",
        mode="lines+markers")
    push!(traces, total_expense_trace)
    savings_trace = PlotlyJS.scatter(; x=date_year,
        y=savings,
        name="Direct Savings",
        mode="lines+markers")
    push!(traces, savings_trace)
    taxes_trace = PlotlyJS.scatter(; x=date_year,
        y=simresults.taxes["Salary (Federal, FICA, State)"],
        name="Taxes (Federal, FICA, State)",
        mode="lines+markers")
    push!(traces, taxes_trace)
    capital_gains_trace = PlotlyJS.scatter(; x=date_year,
        y=simresults.taxes["Capital Gains (long-term)"],
        name="Capital Gains",
        mode="lines+markers")
    push!(traces, capital_gains_trace)

    income_layout = PlotlyJS.Layout(; title="Estimated Annualised Salary, Expenses, and Savings vs. Time",
        xaxis_title="Date",
        yaxis_title="US\$")
    income_plot = PlotlyJS.plot(traces, income_layout)

    expenses_layout = PlotlyJS.Layout(; title="Estimated Average Monthly Expenses vs. Time",
        xaxis_title="Date",
        yaxis_title="US\$")
    expenses_plot_monthly = PlotlyJS.plot(expense_traces_monthly, expenses_layout)
    annual_expenses_layout = PlotlyJS.Layout(; title="Estimated Annual Expenses vs. Time",
        xaxis_title="Date",
        yaxis_title="US\$")
    expenses_plot_annual = PlotlyJS.plot(expense_traces_annual, annual_expenses_layout)
    return income_plot, expenses_plot_monthly, expenses_plot_annual
end

"""
    plotFixedRateMortgage(m::FixedRateMortgageSchedule)

A helper function to visualise results from [`FixedRateMortgageSchedule`](@ref).
"""
function plotFixedRateMortgage(m::FixedRateMortgageSchedule)
    num_months = m.m.loan_term.value
    months = 1:num_months
    principal_trace = PlotlyJS.scatter(; x=months,
        y=cumsum(m.principal_payments),
        name="Principal Payments",
        mode="lines+markers")
    interest_trace = PlotlyJS.scatter(; x=months,
        y=cumsum(m.interest_payments),
        name="Interest Payments",
        mode="lines+markers")
    balance_remaining_trace = PlotlyJS.scatter(; x=months,
        y=m.balance_remaining,
        name="Balance Remaining",
        mode="lines+markers")
    traces = [principal_trace, interest_trace, balance_remaining_trace]
    mortgage_duration = num_months / 12
    plot_title = "$(mortgage_duration)-yr fixed rate mortgage at $(round((m.m.annual_rate * 100), digits=2))% on \$$(m.m.principal), with $(m.m.down_payment * 100)% down. Monthly Payment = \$$(round(m.monthly_payment, digits=2)), Total Cost = \$$(round(m.total_cost, digits=2)), Total Interest = \$$(round(m.total_interest, digits=2))"

    layout = PlotlyJS.Layout(; title=plot_title,
        xaxis_title="Months",
        yaxis_title="US\$")
    PlotlyJS.plot(traces, layout)

    # plot_title = "$(mortgage_duration)-yr fixed rate mortgage at $(m.m.annual_rate * 100)% on \$$(m.m.principal), with $(m.m.down_payment * 100)% down."

    # layout = PlotlyJS.Layout(;title=plot_title,
    #                           xaxis_title="Months",
    #                           yaxis_title="US\$")
    # amortization_schedule = PlotlyJS.plot(traces, layout)

    # table_values = [
    #     "Monthly Payment" "\$$(round(m.monthly_payment, digits=2))"
    #     "Total Cost" "\$$(round(m.total_cost, digits=2))"
    #     "Total Interest" "\$$(round(m.total_interest, digits=2))"
    # ]

    # table_trace = PlotlyJS.table(;cells_values=table_values)
    # data_table = PlotlyJS.plot(table_trace)

    # [amortization_schedule data_table]
end

"""
    plotMonthlyPayments(h::HousePaymentSchedule; display_plots=true)

A helper function to visualise results from [`HousePaymentSchedule`](@ref).
"""
function plotMonthlyPayments(h::HousePaymentSchedule; display_plots=true)
    mp = plotFixedRateMortgage(h.mortgage_schedule)
    keys = Vector{String}()
    vals = Vector{Float64}()

    pi = round(h.mortgage_schedule.monthly_payment, digits=2)
    pt = round(h.property_taxes, digits=2)
    hi = round(h.insurance_payment, digits=2)
    hm = round(h.maintenance, digits=2)
    total = round(h.total, digits=2)

    push!(keys, "Mortgage Payment (Principal & Interest)")
    push!(vals, pi)
    push!(keys, "Property Taxes")
    push!(vals, pt)
    push!(keys, "Homeowner's Insurance")
    push!(vals, hi)
    push!(keys, "Maintenance")
    push!(vals, hm)

    tmp_title = "Monthly Payments: \$$total"
    p = plotpie(keys, vals, tmp_title)

    plots = Dict{String,PlotlyJS.SyncPlot}()
    plots["Monthly Payments"] = p
    plots["Amortization Schedule"] = mp
    if display_plots
        for (_, plot) ∈ plots
            display(plot)
        end
    end
    return plots
end