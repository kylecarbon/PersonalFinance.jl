#=
Copyright [2023] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

default_expense_growth_rate = 0.0
"""
    RecurringExpense(cost, period, start_date)
    RecurringExpense(cost, period, start_date, end_date)
    RecurringExpense(cost, period, start_date, growth_rate)
    RecurringExpense(cost, period, start_date, end_date, growth_rate)

A fixed, recurring cost:
* `cost`: dollar amount of the expense
* `period::Dates.Period`: how often the expense occurs
* `start_date::Date`: when the expense will start occurring
* `end_date::Union{Date,Nothing}`: optionally, when the expense will end
* `growth_rate`: expected annual growth rate for the expense on [0, 1]. Defaults to $(default_expense_growth_rate).
"""
mutable struct RecurringExpense
    cost::Float64
    period::Dates.Period
    start_date::Date
    end_date::Union{Date,Nothing}
    growth_rate::Float64
end

RecurringExpense(cost, period::Dates.Period, start_date::Date) = RecurringExpense(cost, period, start_date, nothing, default_expense_growth_rate)
RecurringExpense(cost, period::Dates.Period, start_date::Date, end_date::Date) = RecurringExpense(cost, period, start_date, end_date, default_expense_growth_rate)
RecurringExpense(cost, period::Dates.Period, start_date::Date, growth_rate::Float64) = RecurringExpense(cost, period, start_date, nothing, growth_rate)

get_start_date(expense::RecurringExpense) = expense.start_date

"""
    getValuePerDay(expense::RecurringExpense, start_date::Date, end_date::Date)

Given a [`RecurringExpense`](@ref), return:
1. a vector of dates on which the expense will occur
1. a vector of values of each expense
"""
function getValuePerDay(expense::RecurringExpense, start_date::Date, end_date::Date)
    expense_end_date = isnothing(expense.end_date) ? end_date : min(end_date, expense.end_date)
    expense_days = findOccurrences(expense.start_date, expense.period, Pair(start_date, expense_end_date))
    expenses = Vector{Float64}()
    for day ∈ expense_days
        push!(expenses, -expense.cost)
    end
    @assert length(expense_days) == length(expenses) "Length of expense_days ($(length(expense_days))) must match length of expenses ($(length(expenses)))."
    return expense_days, expenses
end

"""
    SingleExpense(cost, date)

A fixed, one-time expense. Convenience function that wraps [`RecurringExpense`](@ref).
"""
SingleExpense(cost::Real, date::Date) = RecurringExpense(Float64(cost), Dates.Year(1000), date, date)

function getValuePerYear(expense::RecurringExpense, start_date::Date, end_date::Date)
    dates, expenses = getValuePerDay(expense, start_date, end_date)
    out = Dict{Int,Float64}()
    for (date, expense_val) ∈ zip(dates, expenses)
        year = Dates.year(date)
        if !haskey(out, year)
            out[year] = expense_val
        else
            out[year] += expense_val
        end
    end
    return out
end

function sumAnnualExpenses(expenses::Dict{String,RecurringExpense}, date=Dates.today())
    costs = 0.0
    end_date = date + Dates.Year(1) - Dates.Day(1)
    for (_, expense) ∈ expenses
        _, values = getValuePerDay(expense, date, end_date)
        costs += sum(values)
    end
    return costs
end

function averageRecurringExpensesOverOneYear(expenses::Dict{String,RecurringExpense}, date=Dates.today(); estimate_inflation=false)
    averaged_expenses = Dict{String,Float64}()
    end_date = date + Dates.Year(1) - Dates.Day(1)
    year2ms = Dates.toms(Dates.Year(1))
    for (key, expense) ∈ expenses
        if Dates.toms(expense.period) <= year2ms
            # average over the year
            _, values = getValuePerDay(expense, date, end_date)
            total_cost = -sum(values)
            if estimate_inflation
                num_years = Dates.year(date) - Dates.year(get_start_date(expense))
                if num_years < 0
                    @debug "Tried to apply inflation in the past -- set inflation to 0 instead."
                    num_years = 0
                end
                total_cost *= (1 + expense.growth_rate)^num_years
            end
            averaged_expenses[key] = total_cost / 12
        else
            @warn "Recurring expense \"$key\" is being ignored when estimating average monthly expenses over a year, since it occurs less than once per year on average."
        end
    end
    return averaged_expenses
end

"""
    estimateAnnualExpenses(expense::RecurringExpense, date)

Given an expense and a date, apply the inflation rate to the expense based on its start date.
"""
function estimateAnnualExpenses(expense::RecurringExpense, date=Dates.today())
    end_date = date + Dates.Year(1) - Dates.Day(1)
    _, expenses = getValuePerDay(expense, date, end_date)
    if length(expenses) == 0
        return 0.0
    end
    num_years = (Dates.Year(date) - Dates.Year(get_start_date(expense))).value
    if num_years < 0
        @debug "Tried to apply inflation in the past -- set inflation to 0 instead."
        num_years = 0
    end
    return sum(expenses) * (1 + expense.growth_rate)^num_years
end