# Real Estate

```@index
Pages = ["mortgage.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

```@docs
PersonalFinance.FixedRateMortgage
PersonalFinance.FixedRateMortgageSchedule
PersonalFinance.calculateFixedRateMortgagePayment
PersonalFinance.calculateAmountOwed
PersonalFinance.calculateFixedRateMortgageSchedule
PersonalFinance.HousePayment
PersonalFinance.HousePaymentSchedule
PersonalFinance.calculateMonthlyPayments
```