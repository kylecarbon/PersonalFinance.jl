# Salary

```@index
Pages   = ["salary.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

```@docs
PersonalFinance.SimpleSalary
PersonalFinance.calcAnnualSalary
PersonalFinance.calcPaycheck
PersonalFinance.getSalaryPerDay
```
