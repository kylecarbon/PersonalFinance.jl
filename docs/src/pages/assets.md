# Assets & Liabilities

```@index
Pages   = ["assets.md"]
```
```@meta
DocTestSetup = quote
    using PersonalFinance
end
```

## Assets & Liabilities
```@docs
PersonalFinance.BankAccount
PersonalFinance.SimpleRetirementAsset
PersonalFinance.SimpleLiability
```
## Expenses
```@docs
PersonalFinance.RecurringExpense
PersonalFinance.SingleExpense
PersonalFinance.getValuePerDay
PersonalFinance.estimateAnnualExpenses
```