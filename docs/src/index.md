# PersonalFinance

The package repository is <https://gitlab.com/kylecarbon/PersonalFinance.jl>.

## Quickstart
```
using Pkg
Pkg.add("https://gitlab.com/kylecarbon/PersonalFinance.jl")
using PersonalFinance
```

## Tutorials
```@contents
Pages = ["tutorials/budgeting.md",
         "tutorials/mortgage_example.md",
         "tutorials/retirement_planning.md"]
Depth = 1
```

## APIs
```@contents
Pages = ["pages/assets.md",
         "pages/mortgage.md",
         "pages/salary.md",
         "pages/tax.md",
         "pages/sim.md",
         "pages/plots.md"]
Depth = 1
```