# Retirement Planning
```@example retirement_example
using PersonalFinance
const pf = PersonalFinance
nothing; # hide
```
Simple tutorial to get you started with planning your retirement.

## Set up your profile
First, set up your profile with: name, birthdate, expected retirement age, life expectancy.

```@example retirement_example
retirement_age = 65
life_expectancy = 90
profile = pf.PersonalProfile("Joe", "Joey", "Smith",
                              Dates.Date(1980, 1, 1),
                              retirement_age,
                              life_expectancy)
nothing; # hide
```

## Set up your financial situation
Set up your salary (numbers based on median salary in USA in 2019). This assumes:
* some fixed annual growth for your salary
* your pay period
* start date for your salary (e.g. used to calculate which days you receive a paycheck)

```@example retirement_example
salary_growth_rate = 0.03
pay_period = Dates.Week(2)
start_date = Dates.Date(2019, 1, 1)
salary = pf.SimpleSalary(60000.0, salary_growth_rate, pay_period, start_date)
nothing; # hide
```

Next, add some accounts, expenses, and assets.

```@example retirement_example
accounts = Dict{String,pf.BankAccount}()
expenses = Dict{String,pf.RecurringExpense}()
assets = Dict{String,pf.SimpleRetirementAsset}()
nothing; # hide
```

These are approximate statistics for the S&P 500 over the last ~50 years.

```@example retirement_example
μ = 0.06
σ = 0.185
principal = 50000.0
asset0 = pf.SimpleRetirementAsset(Distributions.Normal(μ, σ), principal, start_date)
assets["S&P 500"] = asset0
nothing; # hide
```

Set up your mortgage.
```@example retirement_example
principal = 250000.0
down = 0.2
annual_rate = 0.045
term = Dates.Year(30)
M = pf.FixedRateMortgage(principal, down, annual_rate, term)

property_tax_rate = 0.0105
insurance_rate = 0.0022
maintenance_rate = 0.01
H = pf.HousePayment(M, property_tax_rate, insurance_rate, maintenance_rate)
house_payments = pf.calculateMonthlyPayments(H)
```
Let's say you're saving to buy a house in 5 years time.
```@example retirement_example
mortgage_start_date = start_date + Dates.Year(5)
expenses["House Monthly Payment"] = pf.RecurringExpense(house_payments.total, Dates.Month(1), mortgage_start_date, mortgage_start_date + M.loan_term, 0.0)
expenses["House Down Payment"] = pf.SingleExpense(principal * down, mortgage_start_date)
expenses["House Closing Costs"] = pf.SingleExpense(principal * .04, mortgage_start_date)
expenses["House Maintenance"] = pf.RecurringExpense(principal * .006666, Dates.Month(6), mortgage_start_date)
nothing; # hide
```

Set up your regular expenses. Note that we set rent to stop on the day we start the Mortgage.
```@example retirement_example
expenses["Rent"] = pf.RecurringExpense(1000, Dates.Month(1), start_date, mortgage_start_date)
expenses["Food"] = pf.RecurringExpense(400, Dates.Month(1), start_date)
expenses["Fun"] = pf.RecurringExpense(200, Dates.Month(1), start_date)
expenses["Internet"] = pf.RecurringExpense(50, Dates.Month(1), start_date)
expenses["Holiday"] = pf.RecurringExpense(500, Dates.Month(6), start_date)
expenses["Christmas Gift"] = pf.RecurringExpense(500, Dates.Year(1), start_date)
nothing; # hide
```

Also, set up a mapping of expenses to assets. This determines from which accounts your expenses are paid.
In this case, we'll pay for the down payment and closing costs by selling off some of our S&P 500 asset.
```@example retirement_example
expenses2assets = Dict{String,String}()
expenses2assets["House Down Payment"] = "S&P 500"
expenses2assets["House Closing Costs"] = "S&P 500"
nothing; # hide
```

## Simulate it
Create a retirement portfolio, simulate it, then plot it.

```@example retirement_example
portfolio = pf.RetirementPortfolio(profile, salary, accounts, expenses, assets, expenses2assets)
simout = pf.simulateRetirement(portfolio, samples = 1000)
p = pf.plotRetirement(simout)
savefig(p["Net Worth"], "retirement_planning.svg"); nothing # hide
savefig(p["Income Plot"], "income.svg"); nothing # hide
savefig(p["Expenses Pie Plot"], "expenses.svg"); nothing # hide
```
```@raw html
<img src="../retirement_planning.svg" width="200%"/>
<img src="../income.svg" width="200%"/>
<img src="../expenses.svg" width="200%"/>
```