#!/usr/bin/env julia
using Pkg

# Set package directory as primary environment for package manager
cur_dir = @__DIR__
Pkg.activate(abspath(joinpath(cur_dir, "../")))
Pkg.instantiate()

import PersonalFinance
const pf = PersonalFinance

import Pkg
Pkg.add("Blink")
using Blink
Blink.AtomShell.install()