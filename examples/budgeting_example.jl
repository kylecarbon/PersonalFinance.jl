using PersonalFinance
const pf = PersonalFinance

# Set up your salary information (this example is based on median salaries in the US).
salary_growth_rate = 0.03
pay_period = Dates.Week(2)
start_date = Dates.Date(2019, 1, 1)
salary = pf.SimpleSalary(70000.0, salary_growth_rate, pay_period, start_date)

# Short-term budgeting considers your bank account and expenses.
accounts = Dict{String,pf.BankAccount}()
accounts["Checking"] = pf.BankAccount(1000, 0.001, Dates.Year(1))

expenses = Dict{String,pf.RecurringExpense}()
expenses["Rent"] = pf.RecurringExpense(1000, Dates.Month(1), start_date)
expenses["Headphones"] = pf.SingleExpense(150, start_date + Dates.Day(5))

# Create a simple portfolio, simulate it, then plot it.
portfolio = pf.SimplePortfolio(salary, accounts, expenses)
out = pf.simulateBudget(portfolio, start_date=start_date, duration=Dates.Month(5))
p1 = pf.plot(out)
pf.summary(portfolio, start_date=start_date)