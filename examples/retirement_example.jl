using PersonalFinance
const pf = PersonalFinance
# # Setting up your profile
# First, set up your profile: provide your pre-tax savings rate, expected retirement age, birthdate, etc.
retirement_age = 65
life_expectancy = 90
profile = pf.PersonalProfile("Joe", "Joey", "Smith", Dates.Date(1980, 1, 1), retirement_age, life_expectancy)

# Here, you can set up your salary.
salary_growth_rate = 0.03
pay_period = Dates.Week(2)
start_date = Dates.Date(2019, 1, 1)
salary = pf.SimpleSalary(80000.0, salary_growth_rate, pay_period, start_date)

# # Set up your portfolio
# First, add some accounts, expenses, and assets.
accounts = Dict{String,pf.BankAccount}()
expenses = Dict{String,pf.RecurringExpense}()
assets = Dict{String,pf.SimpleRetirementAsset}()
expenses2assets = Dict{String,String}()

# ## Retirement Assets
# These are approximate statistics for the S&P 500 over the last ~50 years.
μ = 0.077
σ = 0.186
principal = 15000.0
asset0 = pf.SimpleRetirementAsset(Distributions.Normal(μ, σ), principal, Dates.Date(2018, 1, 1))
assets["S&P 500"] = asset0

# ## Mortgage
# Set up your mortgage information
principal = 400000.0
down = 0.2
annual_rate = 0.045
term = Dates.Year(30)
M = pf.FixedRateMortgage(principal, down, annual_rate, term)

closing_costs = 0.04
property_tax_rate = 0.0105
insurance_rate = 0.0022
maintenance = 0.08
mortgage_start_date = start_date + Dates.Year(10)
H = pf.HousePayment(M, property_tax_rate, insurance_rate, maintenance, mortgage_start_date, closing_costs)
house_payments = pf.calculateMonthlyPayments(H)

expenses["House: Mortgage Payments"] = pf.RecurringExpense(house_payments.mortgage_schedule.monthly_payment, Dates.Month(1), H.start_date, H.start_date + M.loan_term, 0.0)
expenses["House: Down Payment"] = pf.SingleExpense(house_payments.mortgage_schedule.down_payment, H.start_date)
expenses["House: Closing Costs"] = pf.SingleExpense(house_payments.closing_costs, H.start_date)
expenses["House: Property Taxes"] = pf.RecurringExpense(house_payments.property_taxes, Dates.Month(1), H.start_date, H.start_date + M.loan_term, 0.0)
expenses["House: Home Insurance"] = pf.RecurringExpense(house_payments.insurance_payment, Dates.Month(1), H.start_date, H.start_date + M.loan_term)
expenses["House: Maintenance"] = pf.RecurringExpense(house_payments.maintenance, Dates.Month(1), H.start_date, H.start_date + M.loan_term)

expenses2assets["House: Down Payment"] = "S&P 500"
expenses2assets["House: Closing Costs"] = "S&P 500"

# ## Expenses
# Set up your regular expenses
expenses["Rent"] = pf.RecurringExpense(1000, Dates.Month(1), start_date, mortgage_start_date)
expenses["Food"] = pf.RecurringExpense(400, Dates.Month(1), start_date)
expenses["Fun"] = pf.RecurringExpense(200, Dates.Month(1), start_date)
expenses["Internet"] = pf.RecurringExpense(50, Dates.Month(1), start_date)
expenses["Holiday"] = pf.RecurringExpense(500, Dates.Month(6), start_date)
expenses["Christmas Gifts"] = pf.RecurringExpense(500, Dates.Year(1), start_date)

# Create a retirement portfolio, simulate it, then plot it.
pf.plotMonthlyPayments(house_payments)
portfolio = pf.RetirementPortfolio(profile, salary, accounts, expenses, assets, expenses2assets)
out = pf.simulateRetirement(portfolio, samples=50000)
plots = pf.plotRetirement(out, display_plots=true)